package com.folcademy.myfirtsapi.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodbyeController {
    @PostMapping("/goodbye")
    public String goodbye(){
        return "Goodbye world";
    }
}
