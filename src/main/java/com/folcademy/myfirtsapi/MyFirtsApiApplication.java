package com.folcademy.myfirtsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirtsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirtsApiApplication.class, args);
	}

}
